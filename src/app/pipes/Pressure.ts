/**
 * Created by pKozak on 15.04.2017.
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'Pressure'})
export class Pressure implements PipeTransform {
  transform(value:number, args:string[]) : any {
    return (value) ? (value / 100).toFixed(0) : "";
  }
}
