import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import 'rxjs/add/operator/startWith';
import { WeatherInfoService } from '../services/weather-info.service'
import { SensorAddressModel } from '../models/SensorAddressModel'
import { SensorDataModel } from '../models/SensorDataModel'

@Component({
    selector: 'app-sensors',
    templateUrl: './sensors.component.html',
    styleUrls: ['./sensors.component.scss',]
})
export class SensorsComponent implements OnInit {

    sensorCtrl: FormControl;
    sensorAddresses: SensorAddressModel[];
    filteredAddresses: any;
    chosenSensor: SensorAddressModel;
    oneSensorData: SensorDataModel;
    checkFavouriteField = false;
    sensorFavouriteList: SensorAddressModel[] = [];
    favouriteSensorStationsName: SensorAddressModel[] = [];
    favouriteSensorStationsObjects = {};
    //historyData [id: history,...]
    historySensorStationsObjects = {};
    historyOneSensorData = [];

    constructor(private weatherInfoService:WeatherInfoService) {
        this.sensorCtrl = new FormControl();
        this.weatherInfoService.getCurrentSensors().subscribe(data => {
            this.sensorAddresses = data
            this.filteredAddresses = this.sensorCtrl.valueChanges
                .startWith(null)
                .map(name => this.filterSensors(name))
        })
    }

    filterSensors(val: string) {
        return val ? this.sensorAddresses.filter(s => new RegExp(`^${val}`, 'gi').test(s.formattedAddress))
            : this.sensorAddresses;
    }

    displayAddressOnly(sensor: SensorDataModel): string {
      return sensor ? sensor.formattedAddress : "";
   }

    ngOnInit() {
        this.initializeFavouriteList()
    }

    // method fire on change autocomplete input
    onChangeSensorStation(sensor){
        if (this.sensorAddresses != null && this.sensorAddresses.includes(sensor)) {
            // if we have data in favourite list take it
            if (!this.favouriteSensorStationsObjects[sensor.id]) {
                this.weatherInfoService.getSensorData(sensor.id).subscribe(data => {
                    this.oneSensorData = new SensorDataModel(data.currentMeasurements)
                    this.oneSensorData['formattedAddress'] = sensor.formattedAddress;
                    this.oneSensorData['id'] = sensor.id;

                    this.historyOneSensorData[sensor.id] = [];
                    data.history.map((data) => {
                        let historyData = new SensorDataModel(data.measurements)
                        historyData["fromDateTime"] = data["fromDateTime"]
                        historyData["tillDateTime"] = data["tillDateTime"]
                        historyData["id"] = sensor.id
                        historyData["formattedAddress"] = sensor.formattedAddress
                        this.historyOneSensorData[sensor.id].push(historyData)
                    })
                })
            } else {
                this.oneSensorData = new SensorDataModel(this.favouriteSensorStationsObjects[sensor.id])
                this.oneSensorData['formattedAddress'] = this.favouriteSensorStationsObjects[sensor.id].formattedAddress;
                this.oneSensorData['id'] = sensor.id

                this.historyOneSensorData[sensor.id] = this.historySensorStationsObjects[sensor.id];

            }
            // If we change template we must get data from localStorage and initialize our array again
            if (this.sensorFavouriteList.length < 1 && localStorage.getItem('sensorFavouriteList')) {
                this.sensorFavouriteList = JSON.parse(localStorage.getItem('sensorFavouriteList'))
            }
            // handle if checkbox add to Favourite List should be checked or no
            if (this.sensorFavouriteList.some(l => l.id == sensor.id)) {
                this.checkFavouriteField = true
            } else {
                this.checkFavouriteField = false
            }
            return true
        }
        this.checkFavouriteField = false;
    }

    // Changes in Favourite Lists
    onChangeFavouriteList(isChecked, chosenSensor) {
        // If we change template we must get data from localStorage and initialize our array again
        if (this.sensorFavouriteList.length < 1 && localStorage.getItem('sensorFavouriteList')) {
            this.sensorFavouriteList = JSON.parse(localStorage.getItem('sensorFavouriteList'))
        }
        // If button is checked and chosenSensor is defined and chosenSensor is not in array add chosenSensor to array
        if (isChecked && chosenSensor && !this.sensorFavouriteList.some(l => l.id == chosenSensor.id)) {
            this.sensorFavouriteList.push(chosenSensor)
            localStorage.setItem('sensorFavouriteList', JSON.stringify(this.sensorFavouriteList))
            //rerender fsvourite list
            this.addItemToFavouriteList(chosenSensor)
            return true
        }

        // If button is not checked and chosenSensor is defined and chosenSensor is in array, remove chosenSensor from array
        if (!isChecked && chosenSensor && this.sensorFavouriteList.some(l => l.id == chosenSensor.id)) {

            //rerender favourite list
            this.removeItemFromFavouriteList(chosenSensor.id)
            return false
        }
    }

    // Initialize Favourite List
    initializeFavouriteList() {
        // nie potrzebnie ida dodatkowe requesty do danych ktore juz mamy wysietlone - mozna dorobic jakies id i tam sprawdzac czy cos juz mamy zeby nie pukac dodatkowo
        if (localStorage.getItem('sensorFavouriteList')) {
            this.favouriteSensorStationsName = JSON.parse(localStorage.getItem('sensorFavouriteList'))
            this.favouriteSensorStationsName.map(item => {
                    this.addItemToFavouriteList(item)
                })
        }
    }

    removeItemFromFavouriteList(sensorId){
        // if we want to removeItemFromFavouriteList by button we need initalize this.s
        if (this.sensorFavouriteList.length < 1 && localStorage.getItem('sensorFavouriteList')) {
            this.sensorFavouriteList = JSON.parse(localStorage.getItem('sensorFavouriteList'))
        }
        this.sensorFavouriteList = this.sensorFavouriteList.filter(item => item.id != sensorId)
        localStorage.setItem('sensorFavouriteList', JSON.stringify(this.sensorFavouriteList))
        delete this.favouriteSensorStationsObjects[sensorId]
        this.checkFavouriteField = false
    }

    // add item to favourite list
    addItemToFavouriteList(item) {
        //remove request to api if you choose this sensor station
        if(this.oneSensorData && this.oneSensorData['id'] === item.id) {
            this.favouriteSensorStationsObjects[item.id] = new SensorDataModel(this.oneSensorData)
            this.historySensorStationsObjects[item.id] = this.historyOneSensorData[item.id]
            return true
        }

        //remove request to api if we have data in this.favouriteSensorStationsObjects
        if (!this.favouriteSensorStationsObjects[item.id]) {
            //if there is no data call to api
            this.weatherInfoService.getSensorData(item.id).subscribe(
                data => {
                    data.currentMeasurements["formattedAddress"] = item.formattedAddress
                    this.favouriteSensorStationsObjects[item.id] = new SensorDataModel(data.currentMeasurements)

                    this.historySensorStationsObjects[item.id] = []
                    data.history.map((data) => {
                        let historyData = new SensorDataModel(data.measurements)
                        historyData["fromDateTime"] = data["fromDateTime"]
                        historyData["tillDateTime"] = data["tillDateTime"]
                        historyData["id"] = item.id
                        historyData["formattedAddress"] = item.formattedAddress
                        this.historySensorStationsObjects[item.id].push(historyData)
                    })
                }
            )
        }
    }

}
