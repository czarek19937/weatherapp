/**
 * Created by cburas on 17.04.2017.
 */
export class SensorDataModel{
    airQualityIndex: number
    formattedAddress: string
    humidity: number
    id: number
    pm1: number
    pm10: number
    pm25: number
    pollutionLevel: number
    pressure: number
    temperature: number

    constructor(sensorData?:any){
        this.airQualityIndex = sensorData.airQualityIndex
        this.formattedAddress = sensorData.formattedAddress || ""
        this.humidity = sensorData.humidity
        this.pm1 = sensorData.pm1
        this.id = sensorData.id
        this.pm10 = sensorData.pm10
        this.pm25 = sensorData.pm25
        this.pollutionLevel = sensorData.pollutionLevel || 0
        this.pressure = sensorData.pressure
        this.temperature = sensorData.temperature
    }
}
