export class SensorAddressModel{
    id: number;
    formattedAddress: string;

    constructor(sensorData:any){
        this.id = sensorData.id;
        this.formattedAddress = sensorData.formattedAddress
    }
}

export class SensorAddressModelWithLocation{
    id: number;
    formattedAddress: string;
    latitude: number;
    longitude: number;
    distance: number;

    constructor(sensorData:any){
        this.id = sensorData.id;
        this.formattedAddress = sensorData.formattedAddress;
        this.latitude = sensorData.latitude;
        this.longitude = sensorData.longitude;
        this.distance = sensorData.distance;
    }
}