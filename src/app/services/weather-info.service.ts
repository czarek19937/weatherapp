import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { SensorAddressModel, SensorAddressModelWithLocation } from '../models/SensorAddressModel'

@Injectable()
export class WeatherInfoService {
    private allSensorsBaseUrl = 'https://airapi.airly.eu/v1/sensors/current?'
    private allSensorsUrl = 'https://airapi.airly.eu/v1/sensors/current?southwestLat=49.03&southwestLong=14.01&northeastLat=54.97&northeastLong=23.66'
    private allSensorsWithWiosBaseUrl = 'https://airapi.airly.eu/v1/sensorsWithWios/current?'
    private allSensorsWithWiosUrl = 'https://airapi.airly.eu/v1/sensorsWithWios/current?southwestLat=49.03&southwestLong=14.01&northeastLat=54.97&northeastLong=23.66'
    private getSensorDataUrl = 'https://airapi.airly.eu/v1/sensor/measurements?sensorId='
    private getSensorAddressBaseUrl = 'https://airapi.airly.eu/v1/sensors/'
    public sensorAddress: SensorAddressModel[] = [];
    public sensorAddressWithLocation: SensorAddressModelWithLocation[] = [];

    private headers = new Headers({
        'Content-Type': 'application/json',
        'apikey': 'f2758ba85eb7469d9229105cbf959837'
    });

    constructor(private http: Http) {}

    getSensorData(sensorId){
        let options = new RequestOptions({ headers: this.headers});
        return this.http.get(this.getSensorDataUrl + sensorId, options).map((res:Response) => res.json());
    }

    getCurrentSensors() {
        let options = new RequestOptions({ headers: this.headers });
        return this.http.get(this.allSensorsWithWiosUrl, options)
            .map((res:Response) => {
                res.json().map(sensorStation => {
                   if(sensorStation.pollutionLevel != 0){
                        if (sensorStation.address.locality == null) {
                            sensorStation.address.locality = ''
                        }
                        if (sensorStation.address.route == null) {
                            sensorStation.address.route = ''
                        }
                        if (sensorStation.address.streetNumber == null) {
                            sensorStation.address.streetNumber = ''
                        }
                        let provider = (sensorStation.vendor) ? sensorStation.vendor : sensorStation.name;
                        let fullAdressString = sensorStation.address.locality + " " + sensorStation.address.route + " " + sensorStation.address.streetNumber + " - " + provider
                        let fullAdress = {
                            address: fullAdressString
                        };
                        let s = new SensorAddressModel({id: sensorStation.id, formattedAddress: fullAdressString});
                        this.sensorAddress.push(s);
                   }
                })
                return this.sensorAddress
            })
    }

    getOneSensorAddressById(sensorId){
        let options = new RequestOptions({ headers: this.headers });
        let adress: SensorAddressModel
        return this.http.get(this.getSensorAddressBaseUrl + sensorId, options)
            .map((res:Response) => { 
                let object = res.json();
                    if (object.address.locality == null) {
                        object.address.locality = ''
                    }
                    if (object.address.route == null) {
                        object.address.route = ''
                    }
                    if (object.address.streetNumber == null) {
                        object.address.streetNumber = ''
                    }
                    let provider = (object.vendor) ? object.vendor : object.name;
                    let fullAdressString = object.address.locality + " " + object.address.route + " " + object.address.streetNumber + " - " + provider
                    let fullAdress = {
                        address: fullAdressString
                    };

                return new SensorAddressModel({id: object.id, formattedAddress: fullAdressString});
               });
    }

    getNearestSensorsFromSquare(currentLat, currentLong, southwestLat, southwestLong, northeastLat, northeastLong){
         // wyczyść tablice
         this.sensorAddressWithLocation = [];
        //jeśli przesłano wartości null = przeszukaj całą Polske
        if(!southwestLat || !southwestLong || !northeastLat || !northeastLong){
            southwestLat = 49.03
            southwestLong = 14.01
            northeastLat = 54.97
            northeastLong = 23.66
        }

        let options = new RequestOptions({ headers: this.headers });
        return this.http.get(this.allSensorsWithWiosBaseUrl + "southwestLat=" +
                                 southwestLat +"&southwestLong=" + southwestLong + "&northeastLat="+
                                 northeastLat +"&northeastLong=" + northeastLong, options)
            .map((res:Response) => {
                res.json().map(sensorStation => {
                   if(sensorStation.pollutionLevel != 0){
                        if (sensorStation.address.locality == null) {
                            sensorStation.address.locality = ''
                        }
                        if (sensorStation.address.route == null) {
                            sensorStation.address.route = ''
                        }
                        if (sensorStation.address.streetNumber == null) {
                            sensorStation.address.streetNumber = ''
                        }
                        let provider = (sensorStation.vendor) ? sensorStation.vendor : sensorStation.name;
                        let fullAdressString = sensorStation.address.locality + " " + sensorStation.address.route + " " + sensorStation.address.streetNumber + " - " + provider
                        let fullAdress = {
                            address: fullAdressString
                        };
                        let s = new SensorAddressModelWithLocation({id: sensorStation.id, formattedAddress: fullAdressString, latitude: sensorStation.location.latitude, longitude: sensorStation.location.longitude});
                        this.sensorAddressWithLocation.push(s);
                   }
                })
                return this.selectNearestFromArray(currentLat, currentLong);
            })
    }

    selectNearestFromArray(currentLat, currentLong){
        let tempArray: SensorAddressModelWithLocation[] = [];
        // http://stackoverflow.com/a/21623206
        let p = 0.017453292519943295;    // Math.PI / 180
        let c = Math.cos;
        
        this.sensorAddressWithLocation.forEach(element => {
            let a = 0.5 - c((element.latitude - currentLat) * p)/2 + 
                    c(currentLat * p) * c(element.latitude * p) * 
                    (1 - c((element.longitude - currentLong) * p))/2;

            element["distance"] = 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
            tempArray.push(element);
        });

        tempArray.sort(function(a, b) { return a.distance - b.distance; })

        return tempArray.slice(0,3);
    }
}
