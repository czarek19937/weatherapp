import { Component, OnInit } from '@angular/core';
import { WeatherInfoService } from '../services/weather-info.service'
import { SensorAddressModel, SensorAddressModelWithLocation } from '../models/SensorAddressModel'
import { SensorDataModel } from '../models/SensorDataModel'
import { GeoLocationModel } from '../models/GeoLocationModel'

@Component({
    selector: 'favourites',
    templateUrl: './favourites.component.html',
    styleUrls: ['./favourites.component.scss']
})
export class FavouritesComponent implements OnInit {

    SensorStationsObjectsByGeo = {};

    favouriteSensorStationsName: SensorAddressModel[] = [];
    favouriteSensorStationsObjects = {};
    sensorFavouriteList: SensorAddressModel[] = [];
    historySensorStationsObjects = {};

    geoLocation: GeoLocationModel;
    square = [];
    sensorsNotFound = false;

    defaultSensorsIds = [204, 337, 822] //Kraków, Warszawa, Wrocław - tylko sensory Airly


    constructor(private weatherInfoService:WeatherInfoService) { }

    ngOnInit() {
      this.getFavouriteList()
      // Check if there is geoloaction allowed in browser
      if(navigator.geolocation){
          // Próba pobrania pozycji. Jeśli zezwolono wykonuje setGeoPosition, jeśli nie to getDefaultSensors.
        navigator.geolocation.getCurrentPosition(this.setGeoPosition.bind(this), this.getDefaultSensors.bind(this));
      }

    }

    getFavouriteList() {
      if (localStorage.getItem('sensorFavouriteList')) {
          this.favouriteSensorStationsName = JSON.parse(localStorage.getItem('sensorFavouriteList'))
          this.favouriteSensorStationsName.map(item => {
                  this.weatherInfoService.getSensorData(item.id).subscribe(
                      data => {
                          data.currentMeasurements["formattedAddress"] = item.formattedAddress
                          this.favouriteSensorStationsObjects[item.id] = new SensorDataModel(data.currentMeasurements)

                          this.historySensorStationsObjects[item.id] = []
                            data.history.map((data) => {
                            let historyData = new SensorDataModel(data.measurements)
                            historyData["fromDateTime"] = data["fromDateTime"]
                            historyData["tillDateTime"] = data["tillDateTime"]
                            historyData["id"] = item.id
                            historyData["formattedAddress"] = item.formattedAddress
                            this.historySensorStationsObjects[item.id].push(historyData)
                        })
                      }
                  )
          })
      }

    }

    removeItemFromFavouriteList(sensorId){
        // if we want to removeItemFromFavouriteList by button we need initalize this.sensorFavouriteList
        if (this.sensorFavouriteList.length < 1 && localStorage.getItem('sensorFavouriteList')) {
            this.sensorFavouriteList = JSON.parse(localStorage.getItem('sensorFavouriteList'))
        }
        this.sensorFavouriteList = this.sensorFavouriteList.filter(item => item.id != sensorId)
        localStorage.setItem('sensorFavouriteList', JSON.stringify(this.sensorFavouriteList))
        delete this.favouriteSensorStationsObjects[sensorId]
    }
    
    //Setting geoPosition by device localization
    setGeoPosition(position){
      this.geoLocation = position.coords;
      this.square = this.countSquare(this.geoLocation);
      this.getSensorsFromSquare(this.square, this.geoLocation);
    }

    countSquare(geoLocation:any){
          /*decimal
            places   degrees          distance
            -------  -------          --------
            0        1                111  km
            1        0.1              11.1 km
            2        0.01             1.11 km
            3        0.001            111  m
            4        0.0001           11.1 m
            5        0.00001          1.11 m
            6        0.000001         11.1 cm
            7        0.0000001        1.11 cm
            8        0.00000001       1.11 mm
          */

        let sqareBoundLength:number = 1; //in degrees

        if((50.04127 < geoLocation.latitude && geoLocation.latitude < 50.08182) && (19.89349 < geoLocation.longitude && geoLocation.longitude < 19.99649) ||//Cracow Square
            (52.11663 < geoLocation.latitude && geoLocation.latitude < 52.15371) && (21.00037 < geoLocation.longitude && geoLocation.longitude < 21.01822) //Warsaw Square
        )
        {
            sqareBoundLength = 0.05; //in degrees
        }
        console.log("squarebound: " + sqareBoundLength)

        let southWestLat = geoLocation.latitude-(sqareBoundLength/2);
        let southWestLon = geoLocation.longitude-(sqareBoundLength/2);
        let northeastLat = geoLocation.latitude+(sqareBoundLength/2);
        let northeastLon = geoLocation.longitude+(sqareBoundLength/2);

        return [southWestLat, southWestLon, northeastLat, northeastLon];
    }

    getSensorsFromSquare(square: number[], geoLocation: any){
        this.weatherInfoService.getNearestSensorsFromSquare(geoLocation.latitude, geoLocation.longitude, square[0], square[1], square[2], square[3]).subscribe(items => {
            if(items[0]){
                items.forEach(item => {
                    this.weatherInfoService.getSensorData(item.id).subscribe(
                        data => {
                            data.currentMeasurements["formattedAddress"] = item.formattedAddress
                            this.SensorStationsObjectsByGeo[item.id] = new SensorDataModel(data.currentMeasurements)

                            this.historySensorStationsObjects[item.id] = []
                                data.history.map((data) => {
                                let historyData = new SensorDataModel(data.measurements)
                                historyData["fromDateTime"] = data["fromDateTime"]
                                historyData["tillDateTime"] = data["tillDateTime"]
                                historyData["id"] = item.id
                                historyData["formattedAddress"] = item.formattedAddress
                                this.historySensorStationsObjects[item.id].push(historyData)
                            })
                        }
                    )
                });
            }
            else {
                this.sensorsNotFound = true;
                this.getDefaultSensors();
            }
            
        });
    }

    getDefaultSensors(){
        this.defaultSensorsIds.forEach(sensorId => {
            this.weatherInfoService.getOneSensorAddressById(sensorId).subscribe(item =>{
                this.weatherInfoService.getSensorData(item.id).subscribe(data => {
                    data.currentMeasurements["formattedAddress"] = item.formattedAddress
                        this.SensorStationsObjectsByGeo[item.id] = new SensorDataModel(data.currentMeasurements)

                        this.historySensorStationsObjects[item.id] = []
                        data.history.map((data) => {
                        let historyData = new SensorDataModel(data.measurements)
                        historyData["fromDateTime"] = data["fromDateTime"]
                        historyData["tillDateTime"] = data["tillDateTime"]
                        historyData["id"] = item.id
                        historyData["formattedAddress"] = item.formattedAddress
                        this.historySensorStationsObjects[item.id].push(historyData)
                        })
                    })
                });
        });
    }
}
