import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MdDialog, MdDialogConfig, MdDialogRef } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component'

@Component({
  selector: 'weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.scss']
})

export class WeatherCardComponent implements OnInit {

    @Input() sensorAddress: string = "";
    @Input() airQualityIndex: number;
    @Input() humidity: number;
    @Input() pm1: number;
    @Input() pm10: number;
    @Input() pm25: number;
    @Input() pollutionLevel: number = 0;
    @Input() pressure: number;
    @Input() temperature: number;
    @Input() sensorId: number;
    @Input() removeButtonEnabled: boolean = false;
    @Input() sensorHistory;
    @Output() removeItemFromFavouriteList = new EventEmitter();
    constructor(public dialog: MdDialog) {

    }

    openDialog() {
        let config = new MdDialogConfig();
        let dialogRef:MdDialogRef<DialogComponent> = this.dialog.open(DialogComponent, config);
        dialogRef.componentInstance.sensorHistory = this.sensorHistory;
        dialogRef.componentInstance.sensorId = this.sensorId;
        dialogRef.componentInstance.sensorFormattedAddress = this.sensorAddress;
    }

    ngOnInit() {
    }

    removeItemFromFavList(id) {
        this.removeItemFromFavouriteList.emit(id)
    }

    getCSSClasses() {
      let cssClasses;
      if(this.pollutionLevel == 0){
        cssClasses = {
          'panel-gray': true,
        }
      } else
      if(this.pollutionLevel == 1) {
         cssClasses = {
          'panel-green': true,
        }
      } else
        if (this.pollutionLevel == 2 || this.pollutionLevel == 3 || this.pollutionLevel == 4) {
          cssClasses = {
            'panel-yellow': true,
          }
        }
        else {
          cssClasses = {
            'panel-red': true,
          }
        }
      return cssClasses;
  }
}
