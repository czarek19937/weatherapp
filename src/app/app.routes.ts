import { Routes } from '@angular/router';
import { FavouritesComponent } from './favourites/favourites.component';
import { SensorsComponent } from './sensors/sensors.component'

export const routes: Routes = [
    { path: '', redirectTo: 'favourites', pathMatch: 'full' },
    { path: 'sensors', component: SensorsComponent },
    { path: 'favourites', component: FavouritesComponent },
];