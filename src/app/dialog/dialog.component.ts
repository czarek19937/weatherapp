import { Component, OnInit } from '@angular/core';
import {MdDialogRef} from '@angular/material';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
    sensorHistory: {};
    sensorId;
    sensorFormattedAddress;
    constructor(public dialogRef: MdDialogRef<DialogComponent>) {
    }


    ngOnInit() {
  }

}
