import { Component, OnInit, Input } from '@angular/core';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

    airQualityIndexTab = [];
    temperatureTab = [];
    humidityTab = [];
    pm1Tab = [];
    pm10Tab = [];
    pm25Tab = [];
    //pressureTab = []; //Ciśnienie rozjeżdża cały wykres, bo jego wartość jest zbyt duża (~1000), co przy wartościach do 100-200 jest masakrą.
    hoursTab = [];

    error: string;

    @Input() sensorHistory;
    @Input() sensorId;
    @Input() sensorFormattedAddress;
    constructor() {

    }
    options: Object;
    from;
    to;
    onChartSelection (e) {
        this.from = e.originalEvent.xAxis[0].min.toFixed(2);
        this.to = e.originalEvent.xAxis[0].max.toFixed(2);
        console.log(this.from, this.to)
    }

    ngOnInit() {
        try {
            this.createSeries()
                .subscribe(() => {
                    this.options = {
                        title : { text : this.sensorFormattedAddress },
                        chart: { zoomType: 'x'},
                        series: [
                            { name : 'CAQI', data: this.airQualityIndexTab},
                            { name : 'Temperature', data: this.temperatureTab, tooltip: { valueSuffix: ' °C' } },
                            { name : 'Humidity', data: this.humidityTab, tooltip: { valueSuffix: ' %' }},
                            { name : 'PM1', data: this.pm1Tab, tooltip: { valueSuffix: ' μg/m3' } },
                            { name : 'PM10', data: this.pm10Tab, tooltip: { valueSuffix: ' μg/m3' } },
                            { name : 'PM2.5', data: this.pm25Tab, tooltip: { valueSuffix: ' μg/m3' } },
                            //{ name : 'Pressure', data: this.pressureTab }
                        ],
                        xAxis : {
                            //nie widac title? Już widać ;)
                            title: {text: "Hours" },
                            categories: this.hoursTab
                        }
                    };
                })
        }
        catch (e){
            this.error = "Error when loading historical data."
        }
    }

    createSeries() {
        return Observable.create(observer => {
            observer.next(this.sensorHistory[this.sensorId].map((data) => {
                if(data.airQualityIndex){
                    this.airQualityIndexTab.push(Number(data.airQualityIndex.toFixed(0)))
                }
                if(data.temperature){
                    this.temperatureTab.push(Number(data.temperature.toFixed(0)))
                }
                if(data.humidity){
                    this.humidityTab.push(Number(data.humidity.toFixed(0)))
                }
                if(data.pm1){
                    this.pm1Tab.push(Number(data.pm1.toFixed(0)))
                }
                if(data.pm10){
                    this.pm10Tab.push(Number(data.pm10.toFixed(0)))
                }
                if(data.pm25){
                    this.pm25Tab.push(Number(data.pm25.toFixed(0)))
                }
                //if(data.pressure){
                    //this.pressureTab.push(Number((data.pressure / 100).toFixed(0)))
                //}
                this.hoursTab.push(this.localDateTime(data.fromDateTime))
            }));
            observer.complete();
        });

    }

    localDateTime(utcDateTime){
        return new Date(utcDateTime).toLocaleString('pl-pl', {hour: '2-digit', minute: '2-digit', second: '2-digit'});
            // http://stackoverflow.com/a/37649046
    }
}
