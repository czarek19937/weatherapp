import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';

import { AppComponent } from './app.component';

import 'hammerjs';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MdCardModule, MdButtonModule, MdTabsModule, MdAutocompleteModule, MdInputModule, MdCheckboxModule, MdDialogModule} from '@angular/material';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import { WeatherCardComponent } from './weather-card/weather-card.component';

import { WeatherInfoService } from './services/weather-info.service';

import { routes } from './app.routes';
import { FavouritesComponent } from './favourites/favourites.component';
import { SensorsComponent } from './sensors/sensors.component';

import { ObjNgFor } from './pipes/ObjNgFor'
import { Pressure } from './pipes/Pressure';
import { HistoryComponent } from './history/history.component';
import { DialogComponent } from './dialog/dialog.component'

import {TranslateModule, TranslateStaticLoader} from "ng2-translate";
import {TranslateLoader} from 'ng2-translate';

declare var require : any;

export function highchartsFactory() {
return require('highcharts');
}

export function createTranslateLoader(http: Http): TranslateStaticLoader {
	return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    WeatherCardComponent,
    FavouritesComponent,
    SensorsComponent,
    ObjNgFor,
    Pressure,
    HistoryComponent,
    DialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MdCardModule,
    MdButtonModule,
    MdTabsModule,
    MdAutocompleteModule,
    MdInputModule,
    MdCheckboxModule,
    MdDialogModule,
    ChartModule,
    RouterModule.forRoot(routes, {useHash: true}),
    TranslateModule.forRoot({ 
		  provide: TranslateLoader,
		  useFactory: createTranslateLoader,
		  deps: [Http]
	  })
  ],
  providers: [
      WeatherInfoService,
      {
        provide: HighchartsStatic,
        useFactory: highchartsFactory
      }

  ],
  entryComponents: [DialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
