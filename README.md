# README #

This is repository of weatherApp which is based on [Airly API](http://apidocs.airly.eu/).
You can easily get information related to weather see the charts which show you history of changes,
save important stations for you in favourite list. With geolocation you get station which are near you.

### Environment ###
##### To run application:
* download repository - git clone https://czarek19937@bitbucket.org/czarek19937/weatherapp.git
* download dependencies with npm install command
* to your comfort working with this project install globally angular-cli with npm install -g @angular/cli
* to run project locally you can use npm run start or ng serve
* to receive static version ready to deploy(in dist folder) run command ng build --prod

### Technologies and important dependencies ###
* Angular2
* [Angular Material](https://material.angular.io/)
* [Angular2-highcharts](https://github.com/gevgeny/angular2-highcharts)

##### Try live version:
[http://weatherapp.pkozak.net/](http://weatherapp.pkozak.net/)